//Sudoku boards source: https://www.kaggle.com/bryanpark/sudoku?select=sudoku.csv
Table puzzles;

int numPuzzles = 10000;

int[][] puzzle = new int[9][9];
int[][] board = new int[9][9];
int[][] solution = new int[9][9];

IntList boardNumbers = new IntList();

int gameSize, lineSplit;

PVector gameOffset = new PVector();

int backgroundColor = color(185, 185, 185);
int boardColor = color(255);
int highlightColor = color(173, 203, 227);
int defaultNumberColor = color(0);
int inputNumberColor = color(75, 134, 180);
int incorrectNumber = color(255, 0, 0);

PVector selectedIndex = new PVector(0, 0);

boolean cheats = true;
boolean canMove = true;
boolean canPress = true;
boolean gameOver = false;

boolean showingConfirmation = false;
boolean resetBoardConfirmation = false;
boolean newBoardConfirmation = false;

public void setup() {
  size(displayWidth, displayHeight);
  initialiseVariables();
  
  puzzles = loadTable("../../data/sudoku/sudoku-puzzles.csv", "header");
  loadBoard(true);
  
  background(backgroundColor);
  renderGame();
}

void initialiseVariables() {
  gameSize = min(displayWidth, displayHeight);
  gameSize = (int)(gameSize*0.9);
  gameOffset.x = (displayWidth - gameSize)/2;
  gameOffset.y = (displayHeight - gameSize)/2;
  
  lineSplit = gameSize/9;
}

public void draw() { 
  gameOver = checkGameOver();
  
  if (!showingConfirmation && gameOver) {
    drawGameOver();
  }
}

boolean checkGameOver() {
  for (int i = 0; i < board.length; i++) {
    for (int j = 0; j < board.length; j++) {
      if (board[i][j] != solution[i][j]) {
        return false;
      }
    }
  }
  return true;
}
