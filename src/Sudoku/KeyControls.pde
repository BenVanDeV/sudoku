public void keyPressed() {
  if(canPress) {
    checkBoardSettingsKeys();
    
    if(canMove && !gameOver) {
      checkMovementKeys();
    }
    else {
      if(key == BACKSPACE && showingConfirmation) {
        if(!gameOver) {
          canMove = true;
          renderGame();
        }
        resetConfirmations();
      }
    }
    canPress = false;
  }
}

public void keyReleased() {
  canPress = true;
}

void checkBoardSettingsKeys() {
  if(key == 'n') {
    if(newBoardConfirmation) {
      //Create new board
      rebuildBoard(true);
    } 
    else if(!resetBoardConfirmation) {
      //Confirm new board request
      drawConfirmation(false);
      newBoardConfirmation = true;
    }
  }
      
  if(key == 'r') { 
    if(resetBoardConfirmation) {
      //Reset current board
       rebuildBoard(false);
    } 
    else if(!newBoardConfirmation) {
      //Confirm reset board request
      drawConfirmation(true);
      resetBoardConfirmation = true;
    }
  }
}

void checkMovementKeys() {
  if(key == CODED) {
    switch(keyCode) {
      case UP:
        selectedIndex.y = (selectedIndex.y != 0 ? selectedIndex.y - 1 : selectedIndex.y);
        break;
      case LEFT:
        selectedIndex.x = (selectedIndex.x != 0 ? selectedIndex.x - 1 : selectedIndex.x);
        break;
      case DOWN:
        selectedIndex.y = (selectedIndex.y != (puzzle.length - 1) ? selectedIndex.y + 1 : selectedIndex.y);
        break;
      case RIGHT:
        selectedIndex.x = (selectedIndex.x != (puzzle.length - 1) ? selectedIndex.x + 1 : selectedIndex.x);
        break;
    }
  }
  else {
    switch(key) {
      case BACKSPACE:
        changeNumber((int)(selectedIndex.x), (int)(selectedIndex.y), 0);
        break;
      case DELETE:
        changeNumber((int)(selectedIndex.x), (int)(selectedIndex.y), 0);
        break;
      case 'c':
        completeBoard();
        break;
      default:
        fillNumber();
        break;
    }
  }
  renderGame();
}

void fillNumber() {
  //Converts key to number. Taking away 48 lines up 0-9 correctly.
  int number = key - 48;
  if(number >= 0 && number <= 9) {
    changeNumber((int)(selectedIndex.x), (int)(selectedIndex.y), number);
  }
}

void changeNumber(int x, int y, int newNumber) {
  if(puzzle[y][x] == 0) {
    board[y][x] = (board[y][x] == newNumber ? 0 : newNumber);

    renderGame();
  }
}

void completeBoard() {
  if(cheats) {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board.length; j++) {
        board[i][j] = solution[i][j];
        renderGame();
      }
    }
  }
}

void rebuildBoard(boolean type) { 
  //True: New Board
  //False: Reset board
  
  resetConfirmations();
  gameOver = false;
  canMove = true;
  
  loadBoard(type);
}

void resetConfirmations() {
  showingConfirmation = false;
  resetBoardConfirmation = false;
  newBoardConfirmation = false;
}
