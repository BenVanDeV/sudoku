void renderGame() {
  drawBackbox();
  drawHighlights();
  drawBoard(); 
  drawNumbers();
}

void loadBoard(boolean newBoard) {
  if(newBoard) {
    int newNumber = (int)(random(numPuzzles));
    
    //Ensure no puzzles are reused
    while(boardNumbers.hasValue(newNumber)) {
      newNumber = (int)(random(numPuzzles));
    }
    
    boardNumbers.append(newNumber);
  }
  
  TableRow row = puzzles.getRow(boardNumbers.get(boardNumbers.size()-1));
    
  for (int i = 0; i < board.length; i++) {
    for (int j = 0; j < board.length; j++) {
      puzzle[i][j] = row.getString("quizzes").charAt(j + 9*i) - 48;
      solution[i][j] = row.getString("solutions").charAt(j + 9*i) - 48;
      board[i][j] = puzzle[i][j];
    }
  }
  
  renderGame();
  gameOver = false;
}

void drawBackbox() {
  fill(boardColor);
  rectMode(CORNER);
  rect(gameOffset.x, gameOffset.y, gameSize, gameSize);
}

void drawBoard() {
  for(int i = 0; i <= board.length; i++) {
    strokeWeight((i % 3 == 0) ? 3 : 1);
    stroke((i % 3 == 0) ? 0 : 30);
    line(gameOffset.x, gameOffset.y+lineSplit*i, width-gameOffset.x, gameOffset.y+lineSplit*i);    
    line(gameOffset.x+lineSplit*i, gameOffset.y, gameOffset.x+lineSplit*i, height-gameOffset.y);
  }
}
 
void drawNumbers() {
  for(int i = 0; i < board.length; i++) {
    for (int j = 0; j < board.length; j++) {
      if(board[j][i] != 0) 
      { 
        fill((puzzle[j][i] == 0) ? inputNumberColor : defaultNumberColor);
        
        textAlign(CENTER, TOP);
        textSize(gameSize/9);
        text(board[j][i], (float)gameOffset.x+lineSplit*(i+0.5), gameOffset.y+lineSplit*j);
      }
    }
  }
}

void drawHighlights() {
  fill(highlightColor);
  noStroke();
  
  PVector pos = getBoardPos((int)(selectedIndex.x), (int)(selectedIndex.y));
  rect(pos.x, pos.y, lineSplit, lineSplit);
}

PVector getBoardPos(int xIndex, int yIndex) {
  PVector pos = new PVector();
  pos.x = gameOffset.x + xIndex * lineSplit;
  pos.y = gameOffset.y + yIndex * lineSplit; 
  
  return pos;
}

void drawGameOver() {
  drawSettingsBox();
  
  fill(0);
  textAlign(CENTER);
  textSize(gameSize/15);
  text("You Win!", displayWidth/2, displayHeight/2 - gameSize/24);
  
  textSize(gameSize/50);
  text("Press 'n' to start a new game.", displayWidth/2, displayHeight/2);
  text("Press 'r' to start over.", displayWidth/2, displayHeight/2 + gameSize/24);
}

void drawConfirmation(boolean type) {
  showingConfirmation = true;
  canMove = false;

  drawSettingsBox();
  
  fill(0);
  textAlign(CENTER);
  textSize(gameSize/25);
  text("Are you sure?", displayWidth/2, displayHeight/2 - gameSize/24);
  
  textSize(gameSize/50);
  if (type) {
    text("If you want to restart,\npress 'r' again", displayWidth/2, displayHeight/2);    
  } else {
    text("If you want to start a new game,\npress 'n' again", displayWidth/2, displayHeight/2);
  }
  
  text("Press 'BACKSPACE' to cancel", displayWidth/2, displayHeight/2 + gameSize/8);
}

void drawSettingsBox() {
  stroke(0);
  strokeWeight(5);
  fill(highlightColor);
  rectMode(CENTER);
  rect(displayWidth/2, displayHeight/2, gameSize/3, gameSize/3);
}
